# IDS721 Week 10 Mini-Proejct
## Author
Ziyu Shi
## Requirements 
Rust Serverless Transformer Endpoint

- Dockerize Hugging Face Rust transformer
- Deploy container to AWS Lambda
- Implement query endpoint

## AWS Endpoint for request
Please try the following `curl` command to test the Endpoint in terminal:
```
curl https://3yfuld5gnc2vmlh3r7ktaisgom0yvrgy.lambda-url.us-east-1.on.aws/\?text=Hello%20From%20Duke%20University
```
Or try this URL in the browser:
```
https://3yfuld5gnc2vmlh3r7ktaisgom0yvrgy.lambda-url.us-east-1.on.aws/\?text=Hello From Duke University
```
The generated text will appear in several minutes, please wait in paitence.

## Project Steps
### Create Rust Lambda Project
1. Create Rust lambda project by `cargo lambda new <PROJECT_NAME>`.
2. `cd <PROJECT_NAME>` and add necessary dependencies to the `Cargo.toml`. In my project, `rust-llm` has been utilized to implement the text generation task. I choose the model of `bloom-ggml` in `Hugging Face/rustformers`, which can perform well in relatively lightweight models.

### Test locally
1. Use `cargo lambda watch` to launch the function locally.
2. Try the following command to test the function in terminal:
    ```
    curl http://<HOST_NAME>:9000\?text=Hello%20From%20Duke%20University
    ```

### Dockerize the Rust Lambda Function and Deploy the Image to AWS Lambda Using ECR
1. Create the Dockerfile with proper images, the images should contain `cargo-lambda`.
2. Build docker image with:
    ```
    sudo docker build -t <IMAGE_NAME> .
    ```
3. Create a ECR on AWS and bind local Docker with AWS:
    ```
    sudo docker login -u AWS -p $(aws ecr get-login-password --region <YOUR_REGION>) <AWS_ACCOUNT_NUMBER>.dkr.ecr.<YOUR_REGION>.amazonaws.com
    ```
4. Tag the image with the ECR:
    ```
    sudo docker tag <IMAGE>:latest <AWS_ACCOUNT_NUMBER>.dkr.ecr.<YOUR_REGION>.amazonaws.com/<ECR_NAME>:latest
    ```
5. Push the Docker image to the AWS ECR:
    ```
    sudo docker push <AWS_ACCOUNT_NUMBER>.dkr.ecr.<YOUR_REGION>.amazonaws.com/<ECR_NAME>:latest
    ```
Then you can check your docker image in the AWS ECR.

### Create AWS Lambda with Docker Image in ECR
1. Navigate to AWS Lambda and create a new Lambda with the option `Container image`.
2. Enter the newly created Lambda and set the configuration `General Configuration` for the model.
3. Create a new function URL, and to enable the `CORS` and set the `Auth type` to `NONE`.

Now you can test the AWS Lambda with the Function URL.

## Sceenshots
### Test the function locally with the input "hello, I am a student"
![image](images/testlocally.png)

### Test the function with URL with the input "hello, I am a student"
![image](images/testURL.png)

### Build Docker image
![image](images/dockerBuild.png)

### Docker Image pushed onto the ECR
![image](images/ECRImage.png)

### Create AWS Lambda with Docker image
![image](images/awsLambda.png)

### AWS Lambda configuration
![image](images/configure.png)

### Test the query endpoint
![image](images/endpointTesting.png)

### Monitoring status on the AWS Lambda
![image](images/monitor.png)