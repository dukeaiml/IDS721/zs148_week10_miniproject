# Use the cargo-lambda image for building
FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder

# Create a directory for your application
WORKDIR /usr/myapp

# Copy your source code into the container
COPY . .

# Build the Lambda function using cargo-lambda
RUN cargo clean && cargo lambda build --release

# Use a new stage for the final image
# copy artifacts to a clean image
FROM public.ecr.aws/lambda/provided:al2

# Create a directory for your lambda function
WORKDIR /mini10

# Copy the bootstrap binary from the builder stage
COPY --from=builder /usr/myapp/target/lambda/rust-llm/bootstrap ./ 
# Copy the llama model here 
COPY --from=builder /usr/myapp/model/bloom-560m-q5_1-ggjt.bin ./ 

# Set the entrypoint for the Lambda function
ENTRYPOINT ["/mini10/bootstrap"]